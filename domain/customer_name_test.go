package domain

import "testing"

func TestNewCustomerName(t *testing.T) {
	type Test struct {
		name           string
		expectedError  error
		expectedOutput CustomerName
	}

	tests := []Test{
		{"", ErrEmptyCustomerName, ""},
		{"Name", nil, CustomerName("Name")},
	}

	for i, test := range tests {
		out, err := NewCustomerName(test.name)
		if out != test.expectedOutput {
			t.Errorf("Test n°%d: Expected: %q but got: %q", i+1, test.expectedOutput, out)
		}
		if err != test.expectedError {
			t.Errorf("Test n°%d: Expected: %q but got: %q", i+1, test.expectedOutput, out)
		}
	}
}

func TestCustomerName_String(t *testing.T) {
	type Test struct {
		customerName   CustomerName
		expectedOutput string
	}

	tests := []Test{
		{CustomerName("Test"), "Test"},
	}

	for i, test := range tests {
		out := test.customerName.String()
		if out != test.expectedOutput {
			t.Errorf("Test n°%d: Expected: %q but got: %q", i+1, test.expectedOutput, out)
		}
	}
}

func TestCustomerName_Equals(t *testing.T) {
	type Test struct {
		customerName1  CustomerName
		customerName2  CustomerName
		expectedOutput bool
	}

	tests := []Test{
		{CustomerName("Test"), CustomerName("Test"), true},
		{CustomerName("Test"), CustomerName("Name"), false},
	}

	for i, test := range tests {
		out := test.customerName1.Equals(test.customerName2)
		if out != test.expectedOutput {
			t.Errorf("Test n°%d: Expected: %t but got: %t", i+1, test.expectedOutput, out)
		}
	}
}
