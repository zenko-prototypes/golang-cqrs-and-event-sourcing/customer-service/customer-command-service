package domain

import "github.com/google/uuid"

type Event interface {
	Name() string
	Handle(a interface{}) error
}

type BaseEvent struct {
	AggregateID uuid.UUID
}
