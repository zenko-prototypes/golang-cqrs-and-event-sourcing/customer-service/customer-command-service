package domain

type Aggregate struct {
	changes    []Event
	version    int
	deleted    bool
	InnerApply func(e Event) error
}

func NewEmptyAggregate() Aggregate {
	return Aggregate{
		version: 0,
		deleted: false,
	}
}

func (a *Aggregate) applyChangeInternal(e Event, isNew bool) error {
	err := a.InnerApply(e)
	if err != nil {
		return err
	}
	if isNew {
		a.changes = append(a.changes, e)
		a.version += 1
	}
	return nil
}
