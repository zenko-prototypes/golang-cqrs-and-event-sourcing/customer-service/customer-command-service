package domain

type CustomerDeleted struct {
	BaseEvent
}

func NewCustomerDeleted(id CustomerID) CustomerDeleted {
	e := CustomerDeleted{
		BaseEvent: BaseEvent{
			AggregateID: id.UUID(),
		},
	}
	return e
}

func (e CustomerDeleted) Handle(a interface{}) error {
	c := a.(*Customer)
	c.Aggregate.deleted = true
	return nil
}

func (e CustomerDeleted) Name() string {
	return "CustomerDeleted"
}
