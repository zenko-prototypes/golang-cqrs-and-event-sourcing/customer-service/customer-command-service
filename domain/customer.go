package domain

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/EventStore/EventStore-Client-Go/esdb"
	"io"
)

var (
	CustomerESDBPrefix      = "customer_"
	ErrInvalidCustomerState = errors.New("customer: invalid state")
	ErrCustomerDeleted      = fmt.Errorf("%w: customer is deleted", ErrInvalidCustomerState)
)

type Customer struct {
	Aggregate
	ID   CustomerID
	Name CustomerName
}

func NewEmptyCustomer() *Customer {
	c := &(Customer{
		Aggregate: NewEmptyAggregate(),
	})
	c.Aggregate.InnerApply = c.ApplyEvent
	return c
}

func NewCustomerFromEvents(events []Event) (*Customer, error) {
	c := NewEmptyCustomer()
	for _, event := range events {
		err := c.applyChangeInternal(event, false)
		if err != nil {
			return nil, err
		}
	}
	return c, nil
}

func (c *Customer) Create(id CustomerID, name CustomerName) error {
	if c.Aggregate.deleted {
		return ErrCustomerDeleted
	}
	e := NewCustomerCreated(id, name)
	return c.Aggregate.applyChangeInternal(e, true)
}

func (c *Customer) UpdateName(name CustomerName) error {
	if c.Aggregate.deleted {
		return ErrCustomerDeleted
	}
	e := NewCustomerNameUpdated(c.ID, name)
	return c.Aggregate.applyChangeInternal(e, true)
}

func (c *Customer) Delete() error {
	if c.Aggregate.deleted {
		return ErrCustomerDeleted
	}
	e := NewCustomerDeleted(c.ID)
	return c.Aggregate.applyChangeInternal(e, true)
}

func (c *Customer) ApplyEvent(e Event) error {
	return e.Handle(c)
}

func (c *Customer) Save(client *esdb.Client) error {
	for _, e := range c.Aggregate.changes {
		data, err := json.Marshal(e)
		if err != nil {
			return err
		}

		_, err = client.AppendToStream(
			context.Background(),
			CustomerESDBPrefix+c.ID.UUID().String(),
			esdb.AppendToStreamOptions{},
			esdb.EventData{
				ContentType: esdb.JsonContentType,
				EventType:   e.Name(),
				Data:        data,
			})
		if err != nil {
			return err
		}
	}
	c.Aggregate.changes = []Event{}

	return nil
}

func GetCustomerByID(id CustomerID, client *esdb.Client) (*Customer, error) {
	var events []Event

	options := esdb.ReadStreamOptions{
		From:      esdb.Start{},
		Direction: esdb.Forwards,
	}

	stream, err := client.ReadStream(context.Background(), CustomerESDBPrefix+id.UUID().String(), options, 100)
	if err != nil {
		return nil, err
	}

	defer stream.Close()

	for {
		event, err := stream.Recv()
		if errors.Is(err, io.EOF) {
			break
		}

		if err != nil {
			panic(err)
		}

		e, err := NewCustomerEventFromRecorded(event.Event)
		if err != nil {
			panic(err)
		}

		events = append(events, e)
	}

	return NewCustomerFromEvents(events)
}

func NewCustomerEventFromRecorded(event *esdb.RecordedEvent) (Event, error) {
	switch event.EventType {
	case "CustomerCreated":
		e := &CustomerCreated{}
		err := json.Unmarshal(event.Data, e)
		if err != nil {
			return nil, err
		}
		return e, nil
	case "CustomerNameUpdated":
		e := &CustomerNameUpdated{}
		err := json.Unmarshal(event.Data, e)
		if err != nil {
			return nil, err
		}
		return e, nil
	case "CustomerDeleted":
		e := &CustomerDeleted{}
		err := json.Unmarshal(event.Data, e)
		if err != nil {
			return nil, err
		}
		return e, nil
	default:
		return nil, fmt.Errorf("%s is not handled", event.EventType)
	}
}
