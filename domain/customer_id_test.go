package domain

import (
	"github.com/google/uuid"
	"testing"
)

func TestNewCustomerID(t *testing.T) {
	type Test struct {
		id             uuid.UUID
		expectedError  error
		expectedOutput CustomerID
	}

	tests := []Test{
		{uuid.Nil, ErrEmptyCustomerID, CustomerID(uuid.Nil)},
		{uuid.MustParse("0613facf-a59d-4b9b-9fbf-5d1bf8465972"), nil, CustomerID(uuid.MustParse("0613facf-a59d-4b9b-9fbf-5d1bf8465972"))},
	}

	for i, test := range tests {
		out, err := NewCustomerID(test.id)
		if out != test.expectedOutput {
			t.Errorf("Test n°%d: Expected: %q but got: %q", i+1, test.expectedOutput, out)
		}
		if err != test.expectedError {
			t.Errorf("Test n°%d: Expected: %q but got: %q", i+1, test.expectedOutput, out)
		}
	}
}

func TestCustomerID_UUID(t *testing.T) {
	type Test struct {
		customerID     CustomerID
		expectedOutput uuid.UUID
	}

	tests := []Test{
		{CustomerID(uuid.MustParse("0613facf-a59d-4b9b-9fbf-5d1bf8465972")), uuid.MustParse("0613facf-a59d-4b9b-9fbf-5d1bf8465972")},
	}

	for i, test := range tests {
		out := test.customerID.UUID()
		if out != test.expectedOutput {
			t.Errorf("Test n°%d: Expected: %q but got: %q", i+1, test.expectedOutput, out)
		}
	}
}

func TestCustomerID_Equals(t *testing.T) {
	type Test struct {
		customerID1    CustomerID
		customerID2    CustomerID
		expectedOutput bool
	}

	tests := []Test{
		{CustomerID(uuid.MustParse("0613facf-a59d-4b9b-9fbf-5d1bf8465972")), CustomerID(uuid.MustParse("0613facf-a59d-4b9b-9fbf-5d1bf8465972")), true},
		{CustomerID(uuid.MustParse("0613facf-a59d-4b9b-9fbf-5d1bf8465972")), CustomerID(uuid.MustParse("728dbe59-d3f7-4114-80aa-f03bbc34c8b0")), false},
	}

	for i, test := range tests {
		out := test.customerID1.Equals(test.customerID2)
		if out != test.expectedOutput {
			t.Errorf("Test n°%d: Expected: %t but got: %t", i+1, test.expectedOutput, out)
		}
	}
}
