package domain

import (
	"errors"
	"fmt"
)

var (
	ErrInvalidCustomerName = errors.New("customer: could not use invalid name")
	ErrEmptyCustomerName   = fmt.Errorf("%w: cannot be empty", ErrInvalidCustomerName)
)

type CustomerName string

func NewCustomerName(name string) (CustomerName, error) {
	if name == "" {
		return "", ErrEmptyCustomerName
	}
	return CustomerName(name), nil
}

func (n CustomerName) String() string {
	return string(n)
}

func (n CustomerName) Equals(name CustomerName) bool {
	return n.String() == name.String()
}
