package domain

type CustomerNameUpdated struct {
	BaseEvent
	CustomerName CustomerName
}

func NewCustomerNameUpdated(id CustomerID, name CustomerName) CustomerNameUpdated {
	e := CustomerNameUpdated{
		BaseEvent: BaseEvent{
			AggregateID: id.UUID(),
		},
		CustomerName: name,
	}
	return e
}

func (e CustomerNameUpdated) Handle(a interface{}) error {
	c := a.(*Customer)
	c.Name = e.CustomerName
	return nil
}

func (e CustomerNameUpdated) Name() string {
	return "CustomerNameUpdated"
}
