package domain

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

var (
	ErrInvalidCustomerID     = errors.New("customer: could not use invalid id")
	ErrEmptyCustomerID       = fmt.Errorf("%w: cannot be empty", ErrInvalidCustomerID)
	ErrWrongCustomerIDFormat = fmt.Errorf("%w: is not a uuid", ErrInvalidCustomerID)
)

type CustomerID uuid.UUID

func NewCustomerID(id string) (CustomerID, error) {
	if id == "" {
		return CustomerID(uuid.Nil), ErrEmptyCustomerID
	}
	parsedID, err := uuid.Parse(id)
	if err != nil {
		return CustomerID(uuid.Nil), ErrWrongCustomerIDFormat
	}
	return CustomerID(parsedID), nil
}

func (i CustomerID) UUID() uuid.UUID {
	return uuid.UUID(i)
}

func (i CustomerID) String() string {
	return uuid.UUID(i).String()
}

func (i CustomerID) Equals(id CustomerID) bool {
	return i.UUID() == id.UUID()
}
