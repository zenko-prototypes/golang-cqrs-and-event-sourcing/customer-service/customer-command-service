package domain

import "github.com/google/uuid"

type CustomerCreated struct {
	BaseEvent
	CustomerID   uuid.UUID
	CustomerName CustomerName
}

func NewCustomerCreated(customerId CustomerID, name CustomerName) CustomerCreated {
	e := CustomerCreated{
		BaseEvent: BaseEvent{
			AggregateID: customerId.UUID(),
		},
		CustomerID:   customerId.UUID(),
		CustomerName: name,
	}
	return e
}

func (e CustomerCreated) Handle(a interface{}) error {
	c := a.(*Customer)
	c.ID = CustomerID(e.CustomerID)
	c.Name = e.CustomerName
	return nil
}

func (e CustomerCreated) Name() string {
	return "CustomerCreated"
}
