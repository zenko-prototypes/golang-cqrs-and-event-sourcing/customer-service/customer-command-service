package event_store

import (
	"github.com/EventStore/EventStore-Client-Go/esdb"
)

var Client *esdb.Client

func Connect(url string) (*esdb.Client, error) {
	settings, err := esdb.ParseConnectionString(url)

	if err != nil {
		return nil, err
	}

	return esdb.NewClient(settings)
}
