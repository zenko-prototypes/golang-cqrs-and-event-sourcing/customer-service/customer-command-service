package service

import (
	"context"
	"github.com/google/uuid"
	"gitlab.com/zenko-templates/go/customer-command-service/domain"
	event_store "gitlab.com/zenko-templates/go/customer-command-service/event_store_db"
	pb "gitlab.com/zenko-templates/go/customer-command-service/gen/go/customers/v1"
	"google.golang.org/protobuf/types/known/emptypb"
)

type CustomerServer struct {
}

func (s *CustomerServer) CreateCustomer(ctx context.Context, req *pb.CreateCustomerRequest) (*emptypb.Empty, error) {
	c := domain.NewEmptyCustomer()

	id, err := domain.NewCustomerID(uuid.New().String())
	if err != nil {
		return nil, err
	}

	name, err := domain.NewCustomerName(req.Name)
	if err != nil {
		return nil, err
	}

	err = c.Create(id, name)

	err = c.Save(event_store.Client)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, err
}
func (s *CustomerServer) UpdateCustomer(ctx context.Context, req *pb.UpdateCustomerRequest) (*emptypb.Empty, error) {
	name, err := domain.NewCustomerName(req.Name)
	if err != nil {
		return nil, err
	}

	id, err := domain.NewCustomerID(req.Id)
	if err != nil {
		return nil, err
	}

	c, err := domain.GetCustomerByID(id, event_store.Client)
	if err != nil {
		return nil, err
	}

	err = c.UpdateName(name)
	err = c.Save(event_store.Client)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, err
}
func (s *CustomerServer) DeleteCustomer(ctx context.Context, req *pb.DeleteCustomerRequest) (*emptypb.Empty, error) {

	id, err := domain.NewCustomerID(req.Id)
	if err != nil {
		return nil, err
	}

	c, err := domain.GetCustomerByID(id, event_store.Client)
	if err != nil {
		return nil, err
	}

	err = c.Delete()

	err = c.Save(event_store.Client)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, err
}
