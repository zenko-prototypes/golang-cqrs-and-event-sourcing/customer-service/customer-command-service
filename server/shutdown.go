package server

import (
	"fmt"
	"os"
	"os/signal"
)

func (s *Server) Shutdown() error {
	s.grpcServer.Stop()
	return s.listener.Close()
}

func (s *Server) graceful() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	go func() {
		_ = <-c
		fmt.Println("Gracefully shutting down...")
		_ = s.Shutdown()
	}()
}
