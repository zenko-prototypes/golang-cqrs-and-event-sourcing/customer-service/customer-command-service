package server

import "google.golang.org/grpc"

func (s *Server) RegisterService(desc *grpc.ServiceDesc, service interface{}) {
	s.grpcServer.RegisterService(desc, service)
}
