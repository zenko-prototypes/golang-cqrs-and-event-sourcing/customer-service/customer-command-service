# Zenko GRPC server Go Template (to be replaced with <my_project> name)

## Description

Add project description

## How to use this template

```bash
# Clone the repository
git clone git@gitlab.com:zenko-templates/go/grpc-server-templates.git <my_project>

# Delete the git template's data
cd <my_project>
rm -rf .git
```

Then configure your new project accordingly

### Update Api name and version

* [ ] `go.mod`: change `module` attribute

### Finalize bootstrap

```bash
# Initialize your new repository
git init
git remote add origin git@gitlab.com/<my-project>.git
git commit -a -m "Initial commit"
git push -u origin master
```

## Installation

Here some documentations about project setup and configuration

### Requirements

* [Go](https://golang.org/doc/install)
* [Protoc](https://grpc.io/docs/protoc-installation/)
* Go Plugin for Protoc
```bash
# Install the protocol compiler plugins for go
$ go install google.golang.org/protobuf/cmd/protoc-gen-go
$ go install google.golang.org/protobuf/cmd/protoc-gen-go-grpc

# Update the PATH so that the protoc compiler can find the plugins
$ export PATH="$PATH:$(go env GOPATH)/bin"
```
* [Buf](https://docs.buf.build/installation/)

### Install

```bash
$ make install
```

## Running the app

```bash
# development
$ make dev

# production mode
$ make run
```

## Build the app

```bash
$ make build
```

## Test

```bash
$ make test
```