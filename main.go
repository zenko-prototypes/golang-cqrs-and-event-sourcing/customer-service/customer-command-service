package main

import (
	event_store "gitlab.com/zenko-templates/go/customer-command-service/event_store_db"
	customersV1 "gitlab.com/zenko-templates/go/customer-command-service/gen/go/customers/v1"
	"gitlab.com/zenko-templates/go/customer-command-service/server"
	"gitlab.com/zenko-templates/go/customer-command-service/service"
	"log"
)

func main() {
	s := server.New()
	c, err := event_store.Connect("esdb://localhost:2113?tls=false")
	if err != nil {
		log.Fatal(err)
	}

	event_store.Client = c

	s.RegisterService(&customersV1.CustomersService_ServiceDesc, &service.CustomerServer{})

	err = s.ListenAndServeWithGraceful(":8080")
	if err != nil {
		log.Fatal(err)
	}
}
